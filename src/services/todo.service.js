import axios from 'axios';
import { CONFIG } from '../constants/config';

const TODO_CONTROLLER_URL = `${CONFIG.API_URL}/todo`;

export const fetchTodos = (query) => {
    return axios.get(`${TODO_CONTROLLER_URL}?q=${query}`);
}

export const addTodo = (title, description, completed) => {
    return axios.post(TODO_CONTROLLER_URL, {
        title,
        description,
        completed: completed || false,
    });
};
 
export const completeTodo = (todoId) => {
    return axios.put(`${TODO_CONTROLLER_URL}/${todoId}/complete`);
}

export const uncompleteTodo = (todoId) => {
    return axios.put(`${TODO_CONTROLLER_URL}/${todoId}/uncomplete`);
}

export const deleteTodo = (todoId) => {
    return axios.delete(`${TODO_CONTROLLER_URL}/${todoId}`);
}