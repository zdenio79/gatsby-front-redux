import React from 'react';
import { Box, IconButton, ButtonGroup } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import RefreshIcon from '@material-ui/icons/Refresh';
import DeleteIcon from '@material-ui/icons/Delete';

const TodoNetworkError = ({error, setError, fetchTodos}) => {
    if(!error) {
        return null;
    }
    return (
        <Alert 
            severity="error"
            action={
                <Box padding={3}>
                    <ButtonGroup>
                        <IconButton 
                            edge="end" 
                            aria-label="refresh"
                            onClick={() => fetchTodos()}
                        >
                            <RefreshIcon />
                        </IconButton>
                        <IconButton 
                            edge="end" 
                            aria-label="delete"
                            onClick={() => setError('')}
                        >
                            <DeleteIcon />
                        </IconButton>
                    </ButtonGroup>
                </Box>
            }
        >
            <AlertTitle>Błąd</AlertTitle>
            <strong>{ error }</strong>
        </Alert> 
    )
};

export default TodoNetworkError;