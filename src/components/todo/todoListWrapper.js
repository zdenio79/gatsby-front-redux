import React, { useEffect, useState } from 'react';
import { Tabs, Tab } from "@material-ui/core";
import TabPanel from "./tabPanel";
import TodoList from "./todoList";
import { useDispatch, useSelector } from 'react-redux';
import { fetchTodosAction } from '../../action/todo.actions';

const TodoListWrapper = () => {
    const dispatch = useDispatch();
    const [value, setValue] = useState(0);
    const handleChange = (event, newValue) => setValue(newValue);
    const {all, completed, active, searchQuery} = useSelector(state => state.todo);

    useEffect(() => {
        dispatch(fetchTodosAction(searchQuery));
    }, [searchQuery]);


    if(!all.length) {
        return <h1>Brak zadań!</h1>
    }

    return (
        <>
            <div>
                <Tabs 
                    indicatorColor="secondary"
                    textColor="primary"
                    centered
                    value={value} 
                    onChange={handleChange} 
                    aria-label="simple tabs example"
                >
                    <Tab label="Do zrobienia" id="simple-tab-0" aria-controls="simple-tabpanel-0" />
                    <Tab label="Ukończone" id="simple-tab-1" aria-controls="simple-tabpanel-1" />
                </Tabs>
            </div>

            <TabPanel value={value} index={0}>
                <TodoList
                    items={active}
                />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <TodoList
                    items={completed}
                />
            </TabPanel>
        </> 
    )
}

export default TodoListWrapper;