import React from 'react';
import { List } from "@material-ui/core";
import TodoListItem from './todoListItem';

const TodoList = ({ items, onCheckboxToggle, onDelete }) => {
    return (
        <List>
            {items.map((todoItem, key) => (
                <TodoListItem
                    todoItem={todoItem} 
                    index={key}
                    key={key}
                    onCheckboxToggle={onCheckboxToggle}
                    onDelete={onDelete}
                />
            ))}
        </List>
    )
}

export default TodoList;