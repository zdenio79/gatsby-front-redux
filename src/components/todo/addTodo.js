import React, { useState } from 'react';
import styled from 'styled-components';
import { TextField, Checkbox, FormControlLabel, Button } from '@material-ui/core';
import { addTodo } from '../../services/todo.service';

const StyledWrapper = styled.form`
    padding: 30px;
    margin-bottom: 50px;
    button {
        float: right;
    }
`

const initialFormValues = {
    title: '',
    description: '', 
    completed: false
};

const AddTodo = ({ onAdd }) => {
    const [formValues, setFormValues] = useState(initialFormValues);
    const {title, description, completed} = formValues;

    const onSubmit = async (event) => {
        event.preventDefault();
        await addTodo(title, description, completed); 
        setFormValues(initialFormValues);
        onAdd();
    }

    // DO OBSLUGI FORMULARZY NA PRZYSZLOSC
    const onChange = name => event => {
        setFormValues({
            ...formValues,
            [name]: event.target.value,
        });
    }
    
    const onToggleCompleted = () => {
        setFormValues({
            ...formValues,
            completed: !formValues.completed,
        });
    }

    return (
        <StyledWrapper onSubmit={onSubmit}>
            <TextField 
                label="Tytuł"
                fullWidth
                variant="outlined"
                margin="normal"
                value={title}
                onChange={onChange('title')}
                required
            />
            <TextField 
                label="Opis"
                fullWidth
                variant="outlined"
                margin="normal"
                value={description}
                onChange={onChange('description')}
            />
            <div>
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={completed}
                            onChange={onToggleCompleted}
                            color="primary"
                        />
                    }
                    label="Ukończone"                
                />
            </div>
            <Button type="submit" variant="contained" color="primary">
                Dodaj
            </Button>            
        </StyledWrapper>
    )
}


export default AddTodo;