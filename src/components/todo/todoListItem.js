import React from 'react';
import {ListItem, ListItemIcon, Checkbox, ListItemText, ListItemSecondaryAction, IconButton} from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';

const TodoListItem = ({
    todoItem, 
    index,
    onCheckboxToggle,
    onDelete,
}) => {
    return (
        <ListItem>
            <ListItemIcon>
                <Checkbox
                    edge="start"
                    checked={todoItem.completed}
                    tabIndex={-1}
                    disableRipple
                    inputProps={{ 'aria-labelledby': index }}
                    onClick={() => onCheckboxToggle(todoItem, index)}
                />
            </ListItemIcon>
            <ListItemText>{todoItem.title}</ListItemText>
            <ListItemText>{todoItem.description}</ListItemText>
            <ListItemSecondaryAction>
                <IconButton
                    edge="end" 
                    aria-label="delete" 
                    onClick={() => onDelete(todoItem)}
                >
                    <DeleteIcon />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    )
}

export default TodoListItem;