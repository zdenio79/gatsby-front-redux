import React from "react"
import PropTypes from "prop-types"
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { lime } from '@material-ui/core/colors';
import Header from "./header"
import "./layout.css"

const theme = createMuiTheme({
  status: {
    danger: lime[500],
  },
});;

const Layout = ({ children }) => {

  return (
    <ThemeProvider theme={theme}>
      <Header 
        siteTitle="Lista zadań" 
      />
      <main>{children}</main>        
    </ThemeProvider>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  onSearchInputChange: PropTypes.func,
}

export default Layout
