import { CircularProgress } from '@material-ui/core';
import React from 'react';
import styled from 'styled-components';
import PropTypes from "prop-types";

const StyledWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 999999;
    background-color: rgba(255, 255, 255, ${({opacity}) => opacity});
    &.dark-mode {
        background-color: rgba(0, 0, 0, ${({opacity}) => opacity});
    }
`;

const Loader = ({ visible, darkMode, opacity, color, size, thickness, value }) => {
    if(!visible) {
        return null;
    }

    return (
        <StyledWrapper 
            className={darkMode ? 'dark-mode' : ''}
            opacity={opacity}
        >
            <CircularProgress
                color={color}
                size={size}
                thickness={thickness}
                value={value}
            />
        </StyledWrapper>
    )
}

Loader.propTypes = {
    visible: PropTypes.bool,
    darkMode: PropTypes.bool,
    opacity: PropTypes.number,
    color: PropTypes.oneOf([
        'primary',
        'secondary'
    ]),
    size: PropTypes.number,
    thickness: PropTypes.number,
    value: PropTypes.number,
}

Loader.defaultProps = {
    visible: false,
    darkMode: false,
    opacity: 0.5,
    color: 'primary',
    size: 40,
    thickness: 3.6,
    value: 0,
}

export default Loader;