export const userClickedAction = (times = null) => ({
    type: 'USER_CLICKED',
    payload: times,
});