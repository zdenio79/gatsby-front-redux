import { fetchTodos } from "../services/todo.service";

export const SEARCH_INPUT_CHANGE = 'SEARCH_INPUT_CHANGE'
export const FETCH_TODOS = 'FETCH_TODOS';

export const searchInputChangeAction = (searchString) => ({
    type: SEARCH_INPUT_CHANGE,
    payload: searchString,
});

export const fetchTodosAction = (searchString) => ({
    type: FETCH_TODOS,
    payload: fetchTodos(searchString),
}) 