import { combineReducers } from "redux";
import todo from "./todo.reducer";
import test from "./test.reducer";

export default combineReducers({
    todo,
    test,
});