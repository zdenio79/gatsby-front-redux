const initialState = {
    timesClick: 0,
}

export default (state = initialState, action) => {
    switch(action.type) {
        case "USER_CLICKED":
            return {
                ...state,
                timesClick: action.payload ? action.payload : state.timesClick + 1,
            }
        default:
            return state;
    }
}