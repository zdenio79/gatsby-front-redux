import { Fastfood } from "@material-ui/icons";
import { FETCH_TODOS, SEARCH_INPUT_CHANGE } from "../action/todo.actions";
import { fetchTodos } from "../services/todo.service";

const initialState = {
    searchQuery: "",
    all: [],
    completed: [],
    active: [],
}

export default (state = initialState, action) => {
    switch(action.type) {
        case SEARCH_INPUT_CHANGE: 
            return {
                ...state,
                searchQuery: action.payload,
            }
            case `${FETCH_TODOS}_PENDING`:
                return {
                    ...state,
                    loading: true,
                    error: false,
                };
            case `${FETCH_TODOS}_FULFILLED`:
                return {
                    ...state,
                    all: action.payload.data,
                    completed: action.payload.data.filter(todo => todo.completed),
                    active: action.payload.data.filter(todo => !todo.completed),
                    loading: false,
                    error: false,
                };
            case `${FETCH_TODOS}_REJECTED`:
                return {
                    ...state,
                    loading: false,
                    error: true,
                };
            default:
            return state;
    }
}