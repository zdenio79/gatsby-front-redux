import React from 'react';
import { Provider } from 'react-redux';
import promise from 'redux-promise-middleware';
import { applyMiddleware, createStore as reduxCreateStore } from 'redux';
import rootReducer from '../reducers';
import { composeWithDevTools } from 'redux-devtools-extension';

const composeEnhancers = composeWithDevTools({
  // Specify name here, 
  //  actionsBlacklist, actionsCreators and other options if needed
});

const middlewares = [
    promise
];

const createStore = () => (
    reduxCreateStore(
        rootReducer, 
        composeEnhancers(applyMiddleware(...middlewares)),
        )
)
    

const ReduxProvider = ({ element }) => (
    <Provider store={createStore()}>{element}</Provider>
)

export default ReduxProvider;
// export default ({ element }) => <Provider store={createStore()}>{element}</Provider>;