import React from "react"
import { Box, Container, Typography} from "@material-ui/core";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Loader from "../components/loader";
import AddTodo from "../components/todo/addTodo";
import TodoNetworkError from "../components/todo/todoNetworkErrror";
import TodoListWrapper from "../components/todo/todoListWrapper"; 
const IndexPage = () => {      
  return (
    <Layout>
      <SEO title="Home" />
      <Container> 
        <Box pt={3}>
          <Typography variant="h2">Lista zadań</Typography>
        </Box>
        <AddTodo />
        <Loader
          visible={false}
          darkMode={true}
          opacity={0.5}
          size={120}
          color={'secondary'}
        />
        <TodoNetworkError />
        <TodoListWrapper />
      </Container>
    </Layout>
  )
}
export default IndexPage